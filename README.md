# README

This README would normally document whatever steps are necessary to get the
application up and running.

Steps

	Clone the repository using git clone

	Navigate to the project directory using cd clock-in-out

	Install the gems using bundle install

	Create the database using rake db:create

	Run the migrations using rake db:migrate

	Start the app using rails server

