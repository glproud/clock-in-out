class EventsController < ApplicationController

  def index
	@events = Event.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
  end

  def create
  	user = User.find_by(username: params[:event][:username])

  	if user
  		event_type = params[:commit]
  		last_event = Event.where(user_id: user.id).order(created_at: :desc).first

  		if last_event.present? && last_event.event_type == event_type
  			flash.now[:danger] = "#{last_event.username} has already #{last_event.event_type} on #{last_event.created_at}"
  			render 'new'
  		elsif !last_event.present? && event_type == "Clock out"
  			flash.now[:danger] = "You cannot Clock out, Please Clock in first"
  			render 'new'
  		else 
	        @event = Event.new({
	        	username: user.username, 
	        	user_id: user.id, 
	        	event_type: event_type
	        })
	        if @event.save
	        	if last_event.present?
	        		@time_worked = (@event.created_at - last_event.created_at) / 3600
	        	end
	        	
		      	flash.now[:success] = "Success"
	      		render 'show'
		    else
		      render 'new'
		    end
		end
    else
      flash.now[:danger] = 'Invalid Username'
      render 'new'
    end
  end

  def filtered_events
  	@selected_event_type = params[:event][:event_type]
  	@selected_user = params[:event][:user_id]
  	@events = Event.where(events_params).order(created_at: :desc)
  	 				.paginate(page: params[:page], per_page: 10)
  	render action: :index
  end 

  def events_params
  	events_params = {}
    events_params[:user_id] = params[:event][:user_id] unless params[:event][:user_id] == 'all'
    events_params[:event_type] = params[:event][:event_type] unless params[:event][:event_type] == 'all'

  	return events_params
  end


end