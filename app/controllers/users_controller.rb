class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash.now[:success] = "User successfuly created"
      redirect_to home_path
    else
      flash.now[:danger] = "Invalid Username or User already exists"
      render 'new'
    end
  end


  private

	def user_params
		params.require(:user).permit(:username, :password)
	end
end