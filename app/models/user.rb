class User < ApplicationRecord
	has_many :events
	validates :username,  presence: true, uniqueness: true
end
