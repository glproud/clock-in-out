Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
   root   'events#new'
   get    'home'    	    => 'events#new'
   post   'home'    	    => 'events#create'
   get    'events'  	    => 'events#index'
   get    'filtered_events' => 'events#filtered_events'
   get    'signup'  		=> 'users#new'

   resources :users
end
