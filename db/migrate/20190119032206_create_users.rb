class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, unique: true, null: false 
      t.string :password

      t.timestamps null: false
    end
  end
end
